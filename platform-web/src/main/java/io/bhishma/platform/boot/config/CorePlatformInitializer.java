package io.bhishma.platform.boot.config;

import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import io.bhishma.platform.boot.CorePlatformEscapePatterns;

@Import({CorePlatformAutoConfig.class, CorePlatformBootConfig.class , 
	CorePlatformEscapePatterns.class})
public abstract class CorePlatformInitializer extends SpringBootServletInitializer {

}

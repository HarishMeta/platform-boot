package io.bhishma.platform.boot;


import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CorePlatformEscapePatterns {
	
	private Logger LOG = LoggerFactory.getLogger(CorePlatformEscapePatterns.class);
	
	@Value("#{'${auth.skip.urls}'.split(',')}") 
	private Set<String> skipurls;
	
	public boolean contains(HttpServletRequest request) {
		if(request.getMethod().equals("OPTIONS")) {
			return true;
		}
		LOG.info("Servlet path received {} ", request.getServletPath());
		
		if(request.getServletPath().startsWith("/actuator") || 
				request.getServletPath().contains("webjars") || 
				request.getServletPath().contains("swagger-resources") ||
				request.getServletPath().contains("api-docs") ||
				request.getServletPath().contains("csrf") ||
				skipurls.contains(request.getServletPath())) {
			return true;
		}
		return false;
	}


}
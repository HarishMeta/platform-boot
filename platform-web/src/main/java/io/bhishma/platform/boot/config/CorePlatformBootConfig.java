package io.bhishma.platform.boot.config;

import org.springframework.context.annotation.PropertySource;

@PropertySource({"classpath:platform-bootstrap.properties"})
public class CorePlatformBootConfig {

}

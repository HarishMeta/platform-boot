package io.bhishma.platform.boot;

import java.io.Serializable;

public class AuthResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7058746606067480765L;
	private String email;
	private String message;
	private String errorCode;
	private String authToken;

	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}

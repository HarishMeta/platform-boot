package io.bhishma.platform.boot;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CoreExceptionHandler  {

	@ExceptionHandler(AuthException.class)
	@ResponseBody
	public ResponseEntity<ExceptionResponse> authException(AuthException ex) {
		ExceptionResponse response = new ExceptionResponse(ex.getMessage(), ex.getErrorCode());
		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.UNAUTHORIZED);
	}

	// other exception handlers below

}

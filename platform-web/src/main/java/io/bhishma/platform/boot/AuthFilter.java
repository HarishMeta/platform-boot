package io.bhishma.platform.boot;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import io.bhishma.platform.boot.user.User;
import io.bhishma.platform.boot.user.UserContext;

public class AuthFilter extends CorePlatformOncePerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger(AuthFilter.class);

	@Autowired
	private AuthService authService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		LOG.info(request.getHeader("auth-token"));
		LOG.info("URL = " + request.getRequestURL());

		String authHeaders = request.getHeader("auth-token");
		if (StringUtils.isNotBlank(authHeaders)) {
			AuthRequest authRequest = new AuthRequest();
			authRequest.setToken(authHeaders);
			AuthResponse authResponse = authService.getUserByToken(authRequest);
			User user = createUser(authResponse);
			user.setToken(authHeaders);
			UserContext.setUser(user);
			if (null != authResponse && null != authResponse.getEmail()) {
				filterChain.doFilter(request, response);
			} else {
				UserContext.clearUserContext();
				buildMessageForAuthFailure(response, authResponse);
			}
		} else {
			UserContext.clearUserContext();
			AuthResponse authResponse = new AuthResponse();
			authResponse.setErrorCode("E401");
			authResponse.setMessage("Auth token header empty, Please provide a non empty and valid token header");
			buildMessageForAuthFailure(response, authResponse);
		}
	}

	private void buildMessageForAuthFailure(HttpServletResponse response, AuthResponse authResponse) throws IOException {
		LOG.error("Authentication Failure");
		String jsonResponse = new Gson().toJson(authResponse);
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.getOutputStream().println(jsonResponse);
	}

	@Override
	public void destroy() {
		LOG.debug("DestroyFilter--------------");
	}
	
	private User createUser(AuthResponse authResponse) {
		
		final User user = new User();
		user.setEmail(authResponse.getEmail());
		user.setToken(authResponse.getAuthToken());
		
		return user;
		
	}

}

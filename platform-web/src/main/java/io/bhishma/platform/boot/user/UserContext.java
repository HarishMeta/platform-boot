package io.bhishma.platform.boot.user;

public class UserContext {
	
    private static ThreadLocal<User>  userThreadLocal = new ThreadLocal<>();

    public static void setUser(User user){
        if(user != null)
            userThreadLocal.set(user);
    }

    public static User getUser(){
        User userContext = userThreadLocal.get();
        if( userContext == null){
            return new User();
        }
        return userContext;
    }

    public static void clearUserContext(){
        userThreadLocal.remove();
    }


}

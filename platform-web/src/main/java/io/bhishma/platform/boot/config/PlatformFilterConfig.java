package io.bhishma.platform.boot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.client.RestTemplate;

import io.bhishma.platform.boot.AuthFilter;
import io.bhishma.platform.boot.AuthResponseErrorHandler;
import io.bhishma.platform.boot.AuthService;
import io.bhishma.platform.boot.AuthServiceImpl;
import io.bhishma.platform.boot.CorsFilter;

public class PlatformFilterConfig {

	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	@Bean
	@ConditionalOnProperty(value = "auth.enabled", havingValue = "true")
	public FilterRegistrationBean<AuthFilter> authFilter() {

		FilterRegistrationBean<AuthFilter> registration = new FilterRegistrationBean<AuthFilter>();
		AuthFilter authFilter = new AuthFilter();
		beanFactory.autowireBean(authFilter);
		registration.setFilter(authFilter);
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE - 1);
		return registration;
	}

	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter() {
		FilterRegistrationBean<CorsFilter> registration = new FilterRegistrationBean<CorsFilter>();
		CorsFilter corsFilter = new CorsFilter();
		registration.setFilter(corsFilter);
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return registration;
	}
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder
		          .errorHandler(new AuthResponseErrorHandler())
		          .build();
	}
	
	@Bean
	public AuthService authService() {
		return new AuthServiceImpl();
	}


}

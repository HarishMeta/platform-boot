package io.bhishma.platform.boot;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

@Component
public class AuthResponseErrorHandler extends DefaultResponseErrorHandler {
	
	private Logger LOG = LoggerFactory.getLogger(AuthResponseErrorHandler.class);

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		
		if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			LOG.info("Unauthorized attempt for the request. Please provide a valid token");
		} 
	}
}

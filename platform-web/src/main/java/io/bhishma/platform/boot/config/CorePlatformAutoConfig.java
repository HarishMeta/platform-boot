package io.bhishma.platform.boot.config;

import org.springframework.boot.autoconfigure.ImportAutoConfiguration;

@ImportAutoConfiguration(value = { PlatformFilterConfig.class, CorePlatformSecurityConfig.class})
public class CorePlatformAutoConfig {

}

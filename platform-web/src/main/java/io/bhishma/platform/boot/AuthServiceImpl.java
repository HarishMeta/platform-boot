package io.bhishma.platform.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

@Component
public class AuthServiceImpl implements AuthService {
	
	private Logger LOG = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${auth.verify.url}") 
	private String authVerifyUrl;
	
	@Override
	public AuthResponse getUserByToken(AuthRequest authRequest) {
		LOG.info("Auth verify url {} ", authVerifyUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		String jsonRequest = new Gson().toJson(authRequest);
		HttpEntity<String> request = new HttpEntity<String>(jsonRequest, headers);
		ResponseEntity<AuthResponse> response = restTemplate.exchange(authVerifyUrl, HttpMethod.POST, request, AuthResponse.class);
		System.out.println(response);
		return response.getBody();
	}

}

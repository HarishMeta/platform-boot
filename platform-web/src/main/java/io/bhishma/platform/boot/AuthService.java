package io.bhishma.platform.boot;

public interface AuthService {
	
	public AuthResponse getUserByToken(AuthRequest authRequest);

}

package io.bhishma.platform.boot;

public class AuthException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5976426480494321847L;
	private String message;
	private String errorCode;
	
	public AuthException(String message, String errorCode) {
		this.message = message;
		this.errorCode = errorCode;
	}
	
	public String getMessage() {
		return this.message;
	}

	public String getErrorCode() {
		return errorCode;
	}
}

package io.bhishma.platform.boot;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

public abstract class CorePlatformOncePerRequestFilter extends OncePerRequestFilter {

	@Autowired
	private CorePlatformEscapePatterns corePlatformEscapePatterns;

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return corePlatformEscapePatterns.contains(request);
	}
}
